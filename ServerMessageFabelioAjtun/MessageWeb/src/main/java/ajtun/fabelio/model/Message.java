/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ajtun.fabelio.model;

/**
 *
 * @author root
 */
public class Message {

    String msg;
    String tag;

    public Message() {
    }

    public Message(String msg, String tag) {
        this.msg = msg;
        this.tag = tag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
