/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ajtun.fabelio.model;

/**
 *
 * @author root
 */
public class Credential {

    String key;
    String sharedSecret;

    public Credential() {
    }

    public Credential(String key, String sharedSecret) {
        this.key = key;
        this.sharedSecret = sharedSecret;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }
}
