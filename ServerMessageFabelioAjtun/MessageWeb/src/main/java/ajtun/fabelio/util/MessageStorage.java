/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ajtun.fabelio.util;

import ajtun.fabelio.model.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author root
 */
public class MessageStorage {

    private Map<Long, Message> data = new HashMap<>();
    private static MessageStorage messages = new MessageStorage();
    private static Long autoincrementId = 0L;

    public static Long getNextId() {
        return ++autoincrementId;
    }

    public static MessageStorage getInstance() {
        return messages;
    }

    public Message getMessage(Long key) {
        return this.data.get(key);
    }

    public Long puCredential(Message message) {
        Long id = getNextId();
        this.data.put(id, message);
        return id;
    }
    
    public List<Message> getMessageByTag(String tag) {
        List<Message> messagesList = new ArrayList<>();
        for (Map.Entry<Long, Message> messageEntry : this.data.entrySet()) {
            if (tag.equalsIgnoreCase(messageEntry.getValue().getTag())) {
                messagesList.add(messageEntry.getValue());
            }
        }
        return messagesList;
    }
}
