/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ajtun.fabelio.util;

import ajtun.fabelio.model.Credential;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author root
 */
public class DataStorage {

    private Map<String, Credential> data = new HashMap<>();
    private static DataStorage storage = new DataStorage();

    public static DataStorage getInstance() {
        return storage;
    }

    public Credential getCredential(String key) {
        return this.data.get(key);
    }

    public void puCredential(Credential credential) {
        this.data.put(credential.getKey(), credential);
    }

    public boolean exist(String key) {
        return this.data.containsKey(key);
    }
}
