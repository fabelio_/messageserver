/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ajtun.fabelio.rest;

import ajtun.fabelio.model.Message;
import ajtun.fabelio.util.DataStorage;
import ajtun.fabelio.util.MessageStorage;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author root
 */
@Path("/secured")
public class MessageRs {

    private static final Logger Log = Logger.getLogger("MessageRs");
    @Context
    HttpHeaders headers;

    @GET
    @Path("message/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessageById(@PathParam("id") Long id) {

        Response response = autentication("GET /message/<id>", null, Arrays.asList("id:" + id));
        if (response != null) {
            return response;
        }

        return Response.ok(MessageStorage.getInstance().getMessage(id)).build();
    }

    @POST
    @Path("message")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response storageMessage(Message message) {

        Response response = autentication("POST /message",
                Arrays.asList("msg:" + message.getMsg(), "tag:" + message.getTag()), null);
        if (response != null) {
            return response;
        }
        return Response.ok(MessageStorage.getInstance().puCredential(message), MediaType.TEXT_PLAIN)
                .build();
    }

    @GET
    @Path("messages/{tag}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessagesByTag(@PathParam("tag") String tag) {

        Response response = autentication("GET /messages/<tag>", null, Arrays.asList("tag:" + tag));
        if (response != null) {
            return response;
        }
        return Response.ok(MessageStorage.getInstance().getMessageByTag(tag)).build();
    }

    private Response autentication(String path, List<String> body, List<String> PatParams) {
        String key = headers.getHeaderString("X-Key");
        Response.ResponseBuilder builder = Response.status(Response.Status.FORBIDDEN);
        if (key == null || key.isEmpty()) {
            return builder.entity("No se encontre encabezado X-Key").build();
        }

        if (!DataStorage.getInstance().exist(key)) {
            return builder.entity("Key no se encuentra registrado").build();
        }
        String route = headers.getHeaderString("X-Route");
        if (route == null) {
            return builder.entity("No se encontre encabezado X-Route").build();
        }

        if (!path.equals(route)) {
            return builder.entity("Ruta solicitada no coindide con encabezado").build();
        }

        String signature = headers.getHeaderString("X-Signature");
        if (signature == null) {
            return builder.entity("No se encontre encabezado X-Signature").build();
        }

        String signatureCalc = getSignatire(key, route, body, PatParams);

        Log.log(Level.INFO, "Firma calculada");
        Log.log(Level.INFO, signatureCalc);
        if (signatureCalc == null) {
            return builder.entity("Error al calcular HMAC-SHA256 ").build();
        }

        if (!signature.equals(signatureCalc)) {
            return builder.entity("HMAC-SHA256 no coincide").build();
        }
        return null;
    }

    private String getSignatire(String key, String route, List<String> body, List<String> pathParams) {

        String sharedSecret = DataStorage.getInstance().getCredential(key).getSharedSecret();
        List<String> message = new ArrayList<>();
        message.add("X-Route:" + route);
        if (body != null) {
            message.addAll(body);
        }
        if (pathParams != null) {
            message.addAll(pathParams);
        }


        Collections.sort(message, Collator.getInstance(new Locale("es")) );
        Log.log(Level.INFO, "Mensaje a encriptar");
        Log.log(Level.INFO, join(";", message));
        try {
            return String.format("%032x", new BigInteger(1, calcHmacSha256(sharedSecret.getBytes("UTF-8"), join(";", message).getBytes("UTF-8"))));
        } catch (UnsupportedEncodingException ex) {
            return null;
        }
    }

    private byte[] calcHmacSha256(byte[] secretKey, byte[] message) {
        byte[] hmacSha256 = null;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message);
        } catch (Exception e) {
            throw new RuntimeException("Error al calcular HMAC-SHA256 ", e);
        }
        return hmacSha256;
    }

    private static String join(String separator, List<String> input) {

        if (input == null || input.size() <= 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.size(); i++) {

            sb.append(input.get(i));
            if (i != input.size() - 1) {
                sb.append(separator);
            }

        }

        return sb.toString();

    }
}
