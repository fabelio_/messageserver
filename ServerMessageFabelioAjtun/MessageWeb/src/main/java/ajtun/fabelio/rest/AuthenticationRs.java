/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ajtun.fabelio.rest;

import ajtun.fabelio.model.Credential;
import ajtun.fabelio.util.DataStorage;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author root
 */
@Path("/")
public class AuthenticationRs {

    @PUT
    @Path("credential")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response autenticate(Credential credential) {
        if (DataStorage.getInstance().exist(credential.getKey())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        DataStorage.getInstance().puCredential(credential);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
