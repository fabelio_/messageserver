# README #

Para desplegar la aplicacion se deben seguir las siguientes instruciones

1. El proyecto debe ser deplegado en un servidor TomEE version 7.0.0
2. Dentro de la carpeta se encuentra el archivo war que debe ser desplegado. Este se encuentra en la ubicacion
	./ServerMessageFabelioAjtun/MessageWeb/build/libs/message-web.war
3. El servidor debe estar desplegado en el puesto 8080, para que el cliente pude encontrar los servicios.
